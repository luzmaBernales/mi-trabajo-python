import time
import sys
import os
from sys import exit

#contadores
contador=1
intento = 3 
i=0
id_Despacho=0
id=0
costo_huevo=0

#contraseña ADM
pass_adm= '1234'

#LISTAS
gallinas_disponibles = ['GALLINA','PATO','CODORNIZ','AVESTRUZ']
precio_huevos = [50,150,50,800]
precio_actualizados = [0,0,0,0]

#ALMACENADO
datos_despacho = {}

def menuInicial():

    print("\n--------------  \n MENU PRINCIPAL \n --------------")
    print("a .- Asignar precios de Huevos.")
    print("b .- Listar Huevos.")
    print("c .- Crear de Despachos.")
    print("d .- Listar Despachos")
    print("e .- Salir del programa.")
    opcion = input("\nSeleccione una opcion:")

    #PRECIO HUEVOS
    if opcion == 'a':
        preciosHuevos()
        esperar = input("\npresione una tecla para volver a menú principal")

    #LISTR PRECIOS ACTUALIZADOS
    elif opcion == 'b':
        listarHuevos()
        esperar = input("\npresione una tecla para volver a menú principal")

    #INGRESAR DESPACHO
    elif opcion == 'c':
        crearDespacho()
        esperar = input("\npresione una tecla para volver a menú principal")

    #LISTAR DESPACHOS
    elif opcion == 'd':
        listarDespacho()
        esperar = input("\npresione una tecla para volver a menú principal")

    #FINALIZAR
    elif opcion == 'e':
        os._exit(0)
    else:
        print("\n ***** Opción Incorrecta, Intente Nuevamente!!! *****\n")

def preciosHuevos():

    print("\nLOS HUEVOS TIENEN QUE TENER UN PRECIO MAYOR A 50,150,50,800\n")
    
    #gallina
    precio_gallina = int(input("Ingrese precio Gallina:"))
    if precio_huevos[0] <= precio_gallina :
            precio_actualizados[0] = precio_gallina
    else:
        print("dato invalido")

    #pato
    precio_pato = int(input("Ingrese precio Pato:"))
    if precio_huevos[1] <= precio_pato :
        precio_actualizados[1] = precio_pato
    else:
        print("dato invalido")

    #Codorniz
    precio_codorniz = int(input("Ingrese precio Codorniz:"))
    if precio_huevos[2] <= precio_codorniz :
        precio_actualizados[2] = precio_codorniz
    else:
        print("dato invalido")

    #avestruz
    precio_avestruz = int(input("Ingrese precio Avestruz : "))
    if precio_huevos[3] <= precio_avestruz :
        precio_actualizados[3] = precio_avestruz 
    else:
        print("dato invalido")


    print("Detalle de Valores Ingresados: ", precio_actualizados)

def listarHuevos():

        print("\nHUEVOS DISPONIBLES y VALORES:\n")
        print("Huevo Gallina con el precio: ", precio_actualizados[0])
        print("Huevo Pato con el precio: ", precio_actualizados[1])
        print("Huevo Codorniz con el precio: ", precio_actualizados[2])
        print("Huevo Avestruz con el precio: ", precio_actualizados[3])

def crearDespacho():

    #datos ingresados
    rut_cliente = input("Ingrese rut del cliente\n(considere guion,ultimo digito y las respuestas con MAYUSCULAS) \n ")
    razon_social = input("Ingrese su Razon Social\n")
    tipo_huevo = input("Ingrese el tipo de huevo (GALLINA,PATO,CODORNIZ,AVESTRUZ)\n")
    convenio = input("tiene algun convenio? SI/NO \n")
    direc_despacho = input("Ingrese su direccion: \n")
    fecha_despacho = input("Ingresar fecha del despacho (ejemplo: 25-06-2022)\n")
    cant_huevos = int(input("Ingrese cantidad de huevos minimo 50 y maximo 10.000: (ejemplo: 380)\n"))
    

    #datos a calcular
    i = 0
    while i < 3 :
        if(tipo_huevo == gallinas_disponibles[i]):
            costo_huevo = int(cant_huevos) * int(precio_actualizados[i])
            break
        else:
            i = i+1
                
    #reglas de Negocio
    if(rut_cliente == '' or razon_social == '' or tipo_huevo == '' or convenio == '' or fecha_despacho == '' or cant_huevos == ''):
        print("Algun dato incompleto.... Vuelva a intentarlo")   
    elif(int(cant_huevos) < 50 or int(cant_huevos) > 10000):
        print("Cantidad de huevos no esta en el rango.... Vuelva a intentarlo")
                    
    #convenio
    if(convenio == 'SI'):
        descuento = costo_huevo * 0.1
        costo_despacho_total = costo_huevo - descuento
    else:
        costo_despacho_total = costo_huevo
                
    #REGISTRO DESPACHOS
    global id
    id=id+1
    lista = ['despacho']
    lista.append(id)
    lista.append(rut_cliente)
    lista.append(razon_social)
    lista.append(tipo_huevo)
    lista.append(convenio)
    lista.append(direc_despacho)
    lista.append(fecha_despacho)
    lista.append(cant_huevos)
    lista.append(costo_despacho_total)
    datos_despacho[id] = lista
    
    print("\nRegistro almacenado con exito...!\n")

def listarDespacho():

    if (len(datos_despacho) > 0):

        print("-------------- \n SUBMENU DESPACHO\n --------------")
        print("a .- Buscar despacho por RUT")
        print("b .- Buscar despacho por FECHA")
        print("c .- Buscar despacho por NOMBRE O RAZON SOCIAL")
        print("d .- Listar todos los despachos")
        print("e .- Volver al Menú Principal\n")

        OpcionSubMenu = input("\nIngrese una opción:")

        if OpcionSubMenu == "a":
            rutAbuscar = input("ingrese rut a buscar: ")
            buscarRut(rutAbuscar)

        elif OpcionSubMenu == "b":
            fecha_despacho = input("ingrese fecha despacho: ")
            #fecha_despacho = "25-06-2022"
            buscarFECHA(fecha_despacho)

        elif OpcionSubMenu == "c":
            nombre_cliente = input ("ingrese nombre cliente: ")
            buscarNombre(nombre_cliente)    

        elif OpcionSubMenu == "d":
            listarTodoslosDespacho()
            
        elif OpcionSubMenu == "e":
                menuInicial()
        else:
            print("\n ***** Opción Incorrecta, Intente Nuevamente!!! *****\n")

    else:
        print("\n ***** No se han ingresado despachos, Intente Nuevamente!!! *****\n")

def buscarRut(rut):

    rutEncontrado = 0

    i = 1
    while i <= len(datos_despacho) :

        if(rut == datos_despacho[i][2]):

            print("id: ", datos_despacho[i][1])
            print("Rut: ", datos_despacho[i][2])
            print("razon_social: ", datos_despacho[i][3])
            print("Tipo Huevo: ", datos_despacho[i][4])
            print("Tiene Convenio: ", datos_despacho[i][5])
            print("Direc. Despacho: ", datos_despacho[i][6])
            print("Fecha: ", datos_despacho[i][7])
            print("Cantidad Hhuevos: ", datos_despacho[i][8])
            print("Costo Despacho Total: ", datos_despacho[i][9])

            rutEncontrado=1

            break

        else:
            i = i+1

        if (i > len(datos_despacho)) and (rutEncontrado == 0):
            print("\n ***** El Rut no se ha encontrado!!! *****\n")

def buscarFECHA(fecha):

    fechaEncontrada = 0

    i = 1
    while i <= len(datos_despacho) :

        if(fecha == datos_despacho[i][7]):

            print("id: ", datos_despacho[i][1])
            print("Rut: ", datos_despacho[i][2])
            print("razon_social: ", datos_despacho[i][3])
            print("Tipo Huevo: ", datos_despacho[i][4])
            print("Tiene Convenio: ", datos_despacho[i][5])
            print("Direc. Despacho: ", datos_despacho[i][6])
            print("Fecha: ", datos_despacho[i][7])
            print("Cantidad Hhuevos: ", datos_despacho[i][8])
            print("Costo Despacho Total: ", datos_despacho[i][9])

            fechaEncontrada=1

            break

        else:
            i = i+1

        if (i > len(datos_despacho)) and (fechaEncontrada == 0):
            print("\n ***** La fecha no se ha encontrado!!! *****\n")

def buscarNombre(nombre):
        
    nombreEncontrado = 0

    i = 1
    while i <= len(datos_despacho) :

        if(nombre == datos_despacho[i][3]):

            print("id: ", datos_despacho[i][1])
            print("Rut: ", datos_despacho[i][2])
            print("razon_social: ", datos_despacho[i][3])
            print("Tipo Huevo: ", datos_despacho[i][4])
            print("Tiene Convenio: ", datos_despacho[i][5])
            print("Direc. Despacho: ", datos_despacho[i][6])
            print("Fecha: ", datos_despacho[i][7])
            print("Cantidad Hhuevos: ", datos_despacho[i][8])
            print("Costo Despacho Total: ", datos_despacho[i][9])

            nombreEncontrado=1

            break

        else:
            i = i+1

        if (i > len(datos_despacho)) and (nombreEncontrado == 0):
            print("\n ***** EL Nombre no se ha encontrado!!! *****\n")

def listarTodoslosDespacho():
        
    i = 1
    while i <= len(datos_despacho) :
            print("id: ", datos_despacho[i][1])
            print("Rut: ", datos_despacho[i][2])
            print("razon_social: ", datos_despacho[i][3])
            print("Tipo Huevo: ", datos_despacho[i][4])
            print("Tiene Convenio: ", datos_despacho[i][5])
            print("Direc. Despacho: ", datos_despacho[i][6])
            print("Fecha: ", datos_despacho[i][7])
            print("Cantidad Hhuevos: ", datos_despacho[i][8])
            print("Costo Despacho Total: ", datos_despacho[i][9])

            i = i+1

while contador == 1  : 
    #pass_word = input(" Usuario ADM \n Ingrese su contraseña: ")
    #if pass_word == pass_adm :
        menuInicial()
    #else :
        #tiene 3 intentos para ingresar bien la contraseña sino el programa se cierra
        #intento = intento -1 
        #print("Vuelve a intentarlo tienes ", intento, " intentos")
        #if(intento == 0):
            #contador = 0
            #print("--------------")
    # print("programa finalizado")
